[Quran in English](https://www.amazon.com/gp/product/0986136875). Super easy to read. For ages 9 to 99.

[A Quran translation](https://www.clearquran.org) readable even by 4th-grade students.

For children and adults. For Muslims and Non-Muslims.

It uses common vocabulary and simple sentences. It's easy to understand and has a smooth flow. There is no need for explanations and interpretations. It's a direct translation, yet it's super-easy to read.

The readability was scientifically measured with the following methods and results:

* Grammarly readability score: 97%
* Fry Grade Level: 4
* Flesch Kincaid Grade Level: 4.5
* Readable.com score: A

The meaning is highly accurate and doesn't deviate from the meaning of the Arabic text.

It's a non-sectarian translation. It's neither Sunni nor Shia. It just translates the Quran.

The Quran is the word of God, revealed to the Prophet Muhammad�for the benefit of the human being. The Quran contains guidance, healing, and mercy. The Quran is wise, clear, and truthful. The Quran is beyond doubt from the Lord of the Universe.

* For children and adults
* For Muslims and Non-Muslims
* Highly accurate
* Super easy to read and understand
* Clear
* Common vocabulary
* Simple sentences
* Correct grammar
* Correct punctuation
* No interpretations and  explanations
* Nothing in brackets, nothing in parentheses, and no footnotes
* Smooth
* In Modern English

Sura 81

1. When the sun is rolled.

2. And the stars are darkened.

3. And the mountains are set in motion.

4. And the relationships are suspended.

5. And the beasts are gathered.

6. And the oceans are set on fire.

7. And the souls are coupled.

8. And the female, who was buried alive, is asked:

9. For what crime was she killed?

10. And when the pages are made public.

11. And the sky is peeled away.

12. And the Fire is lit.

13. And Paradise is brought near.

14. Every soul will know what it has prepared.

Translated by Talal Itani

First published August 1, 2021